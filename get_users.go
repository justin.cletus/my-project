package main

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	gitlab "github.com/xanzy/go-gitlab"
)

func goDotEnv(key string) string  {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Error loading env file\n")
	}
	return os.Getenv(key)
}

func getUsers() {
	GITLAB_ACCESS_TOKEN := goDotEnv("GITLAB_ACCESS_TOKEN")
	git, err := gitlab.NewClient(GITLAB_ACCESS_TOKEN)
	if err != nil {
		log.Fatalf("failed to create a client %v", err)
	}
	users, _, err := git.Users.ListUsers(&gitlab.ListUsersOptions{})
	if err != nil {
		log.Fatalf("failed to get users %v", err)
	}
	fmt.Printf("-----/n")
	fmt.Println(users)
	fmt.Printf("-----/n")
}
