module github.com/Justincletus003/gitlab-poc

go 1.17

require (
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/xanzy/go-gitlab v0.76.0
	golang.org/x/oauth2 v0.2.0 // indirect
	golang.org/x/time v0.2.0 // indirect
)
