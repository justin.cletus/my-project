package main

import (
	"fmt"
	"log"
	gitlab "github.com/xanzy/go-gitlab"
)

func main() {
	GITLAB_ACCESS_TOKEN := goDotEnv("GITLAB_ACCESS_TOKEN")
	git, err := gitlab.NewClient(GITLAB_ACCESS_TOKEN)
	if err != nil {
		log.Fatalf("failed to create a client %v", err)
	}
	
	p := &gitlab.CreateProjectOptions{
		Name: gitlab.String("My Project"),
		Description: gitlab.String("Demo project for testing"),
		MergeRequestsEnabled: gitlab.Bool(true),
		SnippetsEnabled: gitlab.Bool(true),
		Visibility: gitlab.Visibility(gitlab.PublicVisibility),
	}

	project, _, err := git.Projects.CreateProject(p)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(project.ID)
}
